import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class SpotifyService {
  constructor(private http: HttpClient) {}

  getNewReleases() {
    const headers = new HttpHeaders({
      Authorization:
        "Bearer BQAqeTg5RCHMZdsAHipVMIZJGbzWs18Vl4ROeHuMc6DtaWl0uMB0qJz2hethgwA3htnS-eU0Bg1SqwxAQRE"
    });
    return this.http.get(
      "https://api.spotify.com/v1/browse/new-releases?limits=20",
      {
        headers
      }
    );
  }

  getArtista(termino: string) {
    const headers = new HttpHeaders({
      Authorization:
        "Bearer BQAqeTg5RCHMZdsAHipVMIZJGbzWs18Vl4ROeHuMc6DtaWl0uMB0qJz2hethgwA3htnS-eU0Bg1SqwxAQRE"
    });
    return this.http.get(
      `	https://api.spotify.com/v1/search?q=${termino}&type=artist&limit=15`,
      {
        headers
      }
    );
  }
}
